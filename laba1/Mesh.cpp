#include "Mesh.h"
#include <map>

Mesh::Mesh()
{

}

bool Mesh::load(std::string filename)
{
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	// ������ ��� �������� �������������� ���������
	vector<vec3> v;
	// ������ ��� �������� ��������
	vector<vec3> n;
	// ������ ��� �������� ���������� ���������
	vector<vec2> t;
	// ������ ��� �������� �������� ���������, ��� ���������� ������
	vector<ivec3> fPoints;
	map<string, int> vertexToIndexTable;

	GLuint lastIndex = 0;

	int key = 0;

	ifstream fileInput;
	fileInput.open(filename);
	if (fileInput.is_open()) {
		while (!fileInput.eof()) {
			string st = "";
			float fl1, fl2, fl3;
			int int1, int2, int3;
			fileInput >> st;
			//cout << st << endl;
			//cout << st << endl;
			if (st == "v") {
				fileInput >> fl1;
				fileInput >> fl2;
				fileInput >> fl3;
				v.push_back(vec3(fl1, fl2, fl3));
				//cout << "v\t" << fl1 << '\t' << fl2 << '\t' << fl3 << '\t' << endl;
			}
			if (st == "vn") {
				fileInput >> fl1;
				fileInput >> fl2;
				fileInput >> fl3;
				n.push_back(vec3(fl1, fl2, fl3));
				//cout << "vn\t" << fl1 << '\t' << fl2 << '\t' << fl3 << '\t' << endl;
			}
			if (st == "vt") {
				fileInput >> fl1;
				fileInput >> fl2;
				t.push_back(vec2(fl1, fl2));
				//cout << "vt\t" << fl1 << '\t' << fl2 << '\t' << fl3 << '\t' << endl;
			}
			if (st == "f") {
				for (int i = 0; i < 3; i++) {
					fileInput >> st;

					auto findTemp = vertexToIndexTable.find(st);
					if (findTemp != vertexToIndexTable.end()) { // ���� ������� �������
						// �������� � ������ �������� ��������
						indices.push_back(findTemp->second);
					}
					else { // ���� �� ������� �������
						// �������� � � map
						vertexToIndexTable.insert(pair<string, int>(st, key));

						// �������� ������� � fPoints
						stringstream tempString;
						tempString << st;
						getline(tempString, st, '/');
						int1 = stoi(st);
						getline(tempString, st, '/');
						int2 = stoi(st);
						getline(tempString, st, ' ');
						int3 = stoi(st);
						fPoints.push_back(ivec3(int1, int2, int3));

						// �������� � � ������ ��������
						indices.push_back(key++);
					}
				}
			}
		}
	}
	else
	{
		cout << "err" << endl;
	}
	fileInput.close();

	for (auto& f : fPoints) {
		Vertex temp;
		temp.coord[0] = v[f.x - 1].x;
		temp.coord[1] = v[f.x - 1].y;
		temp.coord[2] = v[f.x - 1].z;
		temp.texCoord[0] = t[f.y - 1].x;
		temp.texCoord[1] = t[f.y - 1].y;
		temp.normal[0] = n[f.z - 1].x;
		temp.normal[1] = n[f.z - 1].y;
		temp.normal[2] = n[f.z - 1].z;
		vertices.push_back(temp);
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertices.size(), vertices.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Vertex) * indices.size(), indices.data(), GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, coord));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texCoord));

	vertexCount = indices.size();

	glBindVertexArray(0);

	return true;
}

void Mesh::drawOne()
{
	glBindVertexArray(vao);

	glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
}
