#include "ResourceManager.h"

using namespace std;
using namespace glm;

int ResourceManager::loadMesh(std::string filename)
{
	auto meshPointer = meshes_id.find(filename);
	int index = 0;
	if (meshPointer == meshes_id.end()){
		Mesh mesh;
		mesh.load(filename);
		meshes.push_back(mesh);
		index = meshes.size() - 1;
		meshes_id.insert(pair<string, int>(filename, index));
	}
	else {
		index = meshPointer->second;
	}
	return index;
}

void ResourceManager::debugOutput()
{
	cout << "---ResourceManager---" << endl;
	cout << "Object count: " << meshes.size() << endl;
	for (auto& obj : meshes_id) {
		cout << obj.first << '\t' << obj.second << endl;
	}
	cout << endl;
}

Mesh* ResourceManager::getMesh(int index)
{
	return &meshes[index];
}
