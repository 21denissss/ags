#include "Camera.h"

using namespace std;
using namespace glm;

Camera::Camera()
{
	eye = vec3(1.0, 13.0, 13.0);
	// �����, � ������� ���������� ������ - (0, 0, 0);
	center = vec3(1, 0, 0);
	// ��������� ������ "�����" (0, 1, 0)
	up = vec3(0, 1, 0);
	
	r = 20;
	offset = vec3(0, 0, 0);
	angleX = 45;
	angleY = 45;

	recalculateViewMatrix();
}

void Camera::setProjectionMatrix(float fovy, float aspect, float zNear, float zFar)
{
	projectionMatrix = perspective(radians(fovy), aspect, zNear, zFar);
}

glm::mat4& Camera::getProjectionMatrix()
{
	return projectionMatrix;
}

glm::mat4& Camera::getViewMatrix()
{
	return viewMatrix;
}

void Camera::moveOXZ(float dx, float dz)
{
	if (dx != 0)
	{
		vec3 cameraDirection = center - eye;
		cameraDirection.y = 0;
		right = normalize(cameraDirection);
		center = center + right * dx;
		eye = eye + right * dx;
		offset = offset + right * dx;
	}

	if (dz != 0)
	{
		vec3 cameraDirection = center - eye;
		cameraDirection.y = 0;
		right = normalize(cross(cameraDirection, up));
		center = center + right * dz;
		eye = eye + right * dz;
		offset = offset + right * dz;
	}
	recalculateViewMatrix();
}

void Camera::rotate(float horizAngle, float vertAngle)
{
	angleY += vertAngle;
	if (angleY <= 10.0)
	{
		angleY = 10.0;
	}
	if (angleY >= 80.0)
	{
		angleY = 80.0;
	}

	angleX += horizAngle;
	if (angleX >= 360)
	{
		angleX -= 360;
	}
	if (angleX <= -360)
	{
		angleX += 360;
	}
	recalculateViewMatrix();
}

void Camera::zoom(float dR)
{
	r += dR;
	if (r < 10)
	{
		r = 10;
	}
	if (r > 50)
	{
		r = 50;
	}
	recalculateViewMatrix();
}

void Camera::recalculateViewMatrix()
{
	eye.x = offset.x + r * sin(radians(angleX)) * sin(radians(angleY));
	eye.y = offset.y + r * cos(radians(angleY));
	eye.z = offset.z + r * sin(radians(angleY)) * cos(radians(angleX));
	viewMatrix = lookAt(eye, center, up);
}
