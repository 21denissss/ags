#pragma once

#include "GL/glew.h"
#include <windows.h>

#include <vector>
#include <iostream>
#include <math.h>


#include <GL/gl.h>
#include <GL/glu.h>
#include "GL/freeglut.h"
#include "../glm/glm/glm.hpp"
#include "../glm/glm/gtc/matrix_transform.hpp"
#include "../glm/glm/gtc/type_ptr.hpp"

using namespace std;

using namespace glm;


// ����� ��� ������ � �������
class Camera
{
public:
	// ����������� �� ���������
	Camera();
	
	// ���������� ������� ��������
	void setProjectionMatrix(float fovy, float aspect, float zNear, float zFar);
	
	// �������� ������� ��������
	glm::mat4& getProjectionMatrix();
	
	// �������� ������� ����
	glm::mat4& getViewMatrix();
	
	// ����������� ������ � ����� ���������� � �������������� ��������� (OXZ)
	void moveOXZ(float dx, float dz);
	
	// ��������� � �������������� � ������������ ��������� (���� �������� � ��������)
	void rotate(float horizAngle, float vertAngle);
	
	// ����������/������� ������ �/�� ����� ����������
	void zoom(float dR);
private:
	// ����������� ������� ����
	void recalculateViewMatrix();
private:
	mat4 projectionMatrix;
	mat4 viewMatrix;
	vec3 eye;
	vec3 center;
	vec3 up;
	// ����������� ��������� ���� ������
	vec3 offset;
	vec3 right;
	float r;
	float angleX;
	float angleY;
};
