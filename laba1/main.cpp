#include <windows.h>
#include "stdio.h"
#include <string>
#include <math.h>
#include <chrono>

#include "GL/glew.h"
#include "GL/freeglut.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include "GL/freeglut.h"

#include "../glm/glm/glm.hpp"
#include "../glm/glm/gtc/matrix_transform.hpp"
#include "../glm/glm/gtc/type_ptr.hpp"

#include "Shader.h"
#include "Camera.h"
#include "GraphicObject.h"
#include "Mesh.h"
#include "ResourceManager.h"


// ������������ ������������ ���� (��� ��������)
using namespace glm;
using namespace std;

// ������������ ������
Camera camera;
// ������������ ������ (���� ������ ����)
Shader shader;
// ������ ����������� �������� ��� ������ �� �����
vector<GraphicObject> graphicObjects;

// ������� ��� ������ �������� � ������� ������� ������� (�� -0.5 �� +0.5)
void drawObject();

// ������� ������ ������ � ������� ��������� �����
// ������ ���������� (x, y, z) �������� �� -0.5 �� +0.5
void drawBox()
{
	// ���������� ��� ������ ������� (�������������� �� ���� �������������)
	static GLuint VAO_Index = 0;	// ������ VAO-������
	static GLuint VBO_Index = 0;	// ������ VBO-������
	static int VertexCount = 0;		// ���������� ������
	static bool init = true;

	if (init) {
		// �������� � ���������� VBO
		glGenBuffers(1, &VBO_Index);
		glBindBuffer(GL_ARRAY_BUFFER, VBO_Index);
		GLfloat Verteces[] = {
			// �������� ����� (��� ������������)
			-0.5, +0.5, +0.5, -0.5, -0.5, +0.5, +0.5, +0.5, +0.5,
			+0.5, +0.5, +0.5, -0.5, -0.5, +0.5, +0.5, -0.5, +0.5,
			// ������ ����� (��� ������������)
			+0.5, +0.5,	-0.5, +0.5, -0.5, -0.5, -0.5, +0.5, -0.5,
			-0.5, +0.5,	-0.5, +0.5, -0.5, -0.5, -0.5, -0.5, -0.5,
			// ������ ����� (��� ������������) 
			+0.5, -0.5,	+0.5, +0.5, -0.5, -0.5, +0.5, +0.5, +0.5,
			+0.5, +0.5,	+0.5, +0.5, -0.5, -0.5, +0.5, +0.5, -0.5,
			// ����� ����� (��� ������������)
			-0.5, +0.5,	+0.5, -0.5, +0.5, -0.5, -0.5, -0.5, +0.5,
			-0.5, -0.5,	+0.5, -0.5, +0.5, -0.5, -0.5, -0.5, -0.5,
			// ������� ����� (��� ������������)
			-0.5, +0.5, -0.5, -0.5, +0.5, +0.5, +0.5, +0.5, -0.5,
			+0.5, +0.5, -0.5, -0.5, +0.5, +0.5, +0.5, +0.5, +0.5,
			// ������ ����� (��� ������������)
			-0.5, -0.5, +0.5, -0.5, -0.5, -0.5, +0.5, -0.5, +0.5,
			+0.5, -0.5, +0.5, -0.5, -0.5, -0.5, +0.5, -0.5, -0.5
		};
		glBufferData(GL_ARRAY_BUFFER, sizeof(Verteces), Verteces, GL_STATIC_DRAW);

		// �������� VAO
		glGenVertexArrays(1, &VAO_Index);
		glBindVertexArray(VAO_Index);

		// ������������� VAO
		glBindBuffer(GL_ARRAY_BUFFER, VBO_Index);
		int location = 0;
		glVertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(location);

		// "�������" ������ VAO �� ������ ������, ���� �������� �� ���������
		glBindVertexArray(0);

		// �������� ���������� ������
		VertexCount = 6 * 6;
		init = false;
	}

	// ����� ������ ������ �� �����
	glBindVertexArray(VAO_Index);
	glDrawArrays(GL_TRIANGLES, 0, VertexCount);
}

// ���������� ��� �������� ���
LARGE_INTEGER prevTime, currTime, incFrequncy;

int getFPS()
{
	static int counter;
	static int oldFps;
	QueryPerformanceCounter(&currTime);
	QueryPerformanceFrequency(&incFrequncy);
	counter++;
	float time = (currTime.QuadPart - prevTime.QuadPart) / incFrequncy.QuadPart;
	if (time >= 1.0) {
		prevTime = currTime;
		oldFps = counter;
		counter = 0;
	}
	return oldFps;
}


// �������, ���������� ��� ��������� �������� ����
void reshape(int w, int h)
{
	// ���������� ����� ������� ���������, ������ ���� ������� ����
	glViewport(0, 0, w, h);
	// ������������� ������� ��������
	camera.setProjectionMatrix(35.0f, (float)w / h, 1.0f, 500.0f);

}

// ��������������� ������� ��� ������������� ����������� ��������
void initGraphicObjects()
{
	// ������ �� �������� �������� (��� ��������)
	ResourceManager& rm = ResourceManager::instance();
	// ��������� ���������� ��� �������� ��������������� ����
	int meshId = -1;
	// ��������� ���������� ��� ������������� ������������ �������
	GraphicObject graphicObject;
	// ���������� ������������ �������
	meshId = rm.loadMesh("MESHES\\buildings\\house_2.obj");
	graphicObject.setMeshId(meshId);
	graphicObject.setColor(vec4(0.2, 0.2, 0.2, 1));
	graphicObject.setPosition(vec3(0, 0, 0));
	graphicObject.setAngle(0.0);
	graphicObjects.push_back(graphicObject);
	// ���������� ������������ �������
	meshId = rm.loadMesh("MESHES\\natures\\big_tree.obj");
	graphicObject.setMeshId(meshId);
	graphicObject.setColor(vec4(0.2, 0.8, 0.2, 1));
	graphicObject.setPosition(vec3(7.5, -0.75, 2.5));
	graphicObject.setAngle(0.0);
	graphicObjects.push_back(graphicObject);
	// ���������� ������������ �������
	meshId = rm.loadMesh("MESHES\\natures\\big_tree.obj");
	graphicObject.setMeshId(meshId);
	graphicObject.setColor(vec4(0.2, 0.8, 0.2, 1));
	graphicObject.setPosition(vec3(-7.5, -0.75, 2.5));
	graphicObject.setAngle(0.0);
	graphicObjects.push_back(graphicObject);
	// ���������� ������������ �������
	meshId = rm.loadMesh("MESHES\\vehicles\\police_car.obj");
	graphicObject.setMeshId(meshId);
	graphicObject.setColor(vec4(0.2, 0.2, 1.0, 1));
	graphicObject.setPosition(vec3(+4.5, -2.15, +6.5));
	graphicObject.setAngle(-115.0);
	graphicObjects.push_back(graphicObject);
	// ���������� ������������ �������
	meshId = rm.loadMesh("MESHES\\vehicles\\police_car.obj");
	graphicObject.setMeshId(meshId);
	graphicObject.setColor(vec4(0.23, 0.23, 1.0, 1));
	graphicObject.setPosition(vec3(+4.25, -2.15, +10.5));
	graphicObject.setAngle(+105.0);
	graphicObjects.push_back(graphicObject);
	// ���������� ������������ �������
	meshId = rm.loadMesh("MESHES\\vehicles\\jeep.obj");
	graphicObject.setMeshId(meshId);
	graphicObject.setColor(vec4(0.95, 0.13, 0.13, 1));
	graphicObject.setPosition(vec3(-1.25, -2.15, +9.0));
	graphicObject.setAngle(+170.0);
	graphicObjects.push_back(graphicObject);
}

// ������� ���������� ��� ����������� ����
// � ��� ����� � �������������, �� ������� glutPostRedisplay
void display()
{
	// ������� ������ �����
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// ��������� ����� ������� (�� ������ ������)
	glEnable(GL_DEPTH_TEST);
	// ����� ��������� � ���� ����� � ���������� ��������� ������
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	// ���������� ������, ������������ ��� ������ �������
	shader.activate();
	// ������������� ������� ��������
	mat4& projectionMatrix = camera.getProjectionMatrix();
	shader.setUniform("projectionMatrix", projectionMatrix);
	// �������� ������� ������
	mat4& viewMatrix = camera.getViewMatrix();
	// ������� ��� �������
	for (auto& graphicObject : graphicObjects) {
		// ������������� ������� ���������� ������
		mat4 modelViewMatrix = viewMatrix * graphicObject.getModelMatrix();
		shader.setUniform("modelViewMatrix", modelViewMatrix);
		// ������������� ����
		shader.setUniform("color", graphicObject.getColor());
		// ������� ���
		int meshId = graphicObject.getMeshId();
		Mesh* mesh = ResourceManager::instance().getMesh(meshId);
		if (mesh != nullptr) mesh->drawOne();
	}
	// ������ �������� � ������ ������ �����
	glutSwapBuffers();
	// ���������� FPS � ��� ����� � ��������� ����
	char windowTitle[128];
	int FPS = getFPS();
	sprintf_s(windowTitle, 128, "Laba_04 [%i FPS]", FPS);
	glutSetWindowTitle(windowTitle);
}

float getSimulationTime()
{
	static auto prevTime = chrono::steady_clock::now();

	auto now = chrono::steady_clock::now();
	chrono::duration<float> delta = now - prevTime;
	prevTime = now;

	return delta.count();
}

// ������� ��������� �������� �������� �����
void mouseWheel(int wheel, int direction, int x, int y)
{
	float delta = -direction * 2;
	camera.zoom(delta);
}

// ������� ���������� ����� ��������� �����������, �.�. ����������� �����
void simulation()
{
	float simulationTime = getSimulationTime();

	static POINT p_start;
	static POINT p_end;
	p_start = p_end;
	GetCursorPos(&p_end);
	if (GetAsyncKeyState(VK_RBUTTON)) {
		camera.rotate(p_end.x - p_start.x, p_end.y - p_start.y);
	};

	if (GetAsyncKeyState(VK_LEFT)) {
		camera.rotate(-40 * simulationTime, 0);
	}
	if (GetAsyncKeyState(VK_RIGHT)) {
		camera.rotate(40 * simulationTime, 0);
	}
	if (GetAsyncKeyState(VK_UP)) {
		camera.rotate(0, -30 * simulationTime);
	}
	if (GetAsyncKeyState(VK_DOWN)) {
		camera.rotate(0, 30 * simulationTime);
	}
	if (GetAsyncKeyState(0x53)) {
		camera.moveOXZ(-30 * simulationTime, 0);
	}
	if (GetAsyncKeyState(0x57)) {
		camera.moveOXZ(30 * simulationTime, 0);
	}
	if (GetAsyncKeyState(0x41)) {
		camera.moveOXZ(0, -30 * simulationTime);
	}
	if (GetAsyncKeyState(0x44)) {
		camera.moveOXZ(0, 30 * simulationTime);
	}

	// ������������ ����
	glutPostRedisplay();
};

void keybord_func(unsigned char sym, int x, int y)
{
	if (sym == ' ') {
		ResourceManager::instance().debugOutput();
	}
}

// �������� �������
void main(int argc, char** argv)
{
	// ��������� ������� ��������
	camera.setProjectionMatrix(35.0, 800.0 / 600.0, 1.0, 100.0);
	// ������������� ���������� GLUT
	glutInit(&argc, argv);
	// ������������� ������� (������ ������)
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_STENCIL | GLUT_MULTISAMPLE);
	// ���������� � ������ OpenGL (������ 3.3 ��� ��������� �������� �������������)
	glutInitContextVersion(3, 3);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	// ������������� ������� ����� ���� ����
	glutInitWindowPosition(300, 100);
	// ������������� ������ ����
	glutInitWindowSize(800, 600);
	// �������� ����
	glutCreateWindow("laba_01");

	// ������������� GLEW 
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		fprintf(stderr, "Glew error: %s\n", glewGetErrorString(err));
		return;
	}
	initGraphicObjects();

	// ����������� ������� ������ OpenGL
	printf("OpenGL Version = %s\n\n", glGetString(GL_VERSION));

	// �������� �������
	shader.load("SHADER\\Example.vsh", "SHADER\\Example.fsh");

	// ������������� �������, ������� ����� ���������� ��� ����������� ����
	glutDisplayFunc(display);
	// ������������� �������, ������� ����� ���������� ��� ��������� �������� ����
	glutReshapeFunc(reshape);
	// ������������� ������� ������� ���������� ������ ���, ����� ��������� �����������
	glutIdleFunc(simulation);
	glutMouseWheelFunc(mouseWheel);
	glutKeyboardFunc(keybord_func);
	// �������� ���� ��������� ��������� ��
	glutMainLoop();
	return;
}

// ������� ��� ������ �������� � ������� ������� ������� (�� -0.5 �� +0.5)
void drawObject()
{
	// ���������� ��� ������ ������� (�������������� �� ���� �������������)
	static bool init = true;
	static GLuint VAO_Index = 0;		// ������ VAO-������
	static GLuint VBO_Index = 0;		// ������ VBO-������
	static int VertexCount = 0;			// ���������� ������

	// ��� ������ ������ �������������� VBO � VAO
	if (init) {
		init = false;
		// �������� � ���������� VBO
		glGenBuffers(1, &VBO_Index);
		glBindBuffer(GL_ARRAY_BUFFER, VBO_Index);
		GLfloat	Verteces[] = {
			-0.5,	+0.5,
			-0.5,	-0.5,
			+0.5,	+0.5,
			+0.5,	+0.5,
			-0.5,	-0.5,
			+0.5,	-0.5
		};
		glBufferData(GL_ARRAY_BUFFER, sizeof(Verteces), Verteces, GL_STATIC_DRAW);

		// �������� VAO
		glGenVertexArrays(1, &VAO_Index);
		glBindVertexArray(VAO_Index);
		// ���������� VAO
		glBindBuffer(GL_ARRAY_BUFFER, VBO_Index);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
		// "�������" ������ VAO, ���� �������� �� ���������
		glBindVertexArray(0);

		// �������� ���������� ������
		VertexCount = 6;
	}

	// ������� �������������
	glBindVertexArray(VAO_Index);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}