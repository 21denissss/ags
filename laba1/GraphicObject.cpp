#define _USE_MATH_DEFINES
#include "GraphicObject.h"
#include <math.h>


using namespace std;
using namespace glm;

GraphicObject::GraphicObject()
{
	// ������ ������������� � ����� (1,0,0) ��� ��������
	modelMatrix = mat4(
		vec4(1, 0, 0, 0), // 1-�� �������: ����������� ��� X
		vec4(0, 1, 0, 0), // 2-�� �������: ����������� ��� Y
		vec4(0, 0, 1, 0), // 3-�� �������: ����������� ��� Z
		vec4(1, 0, 0, 1)); // 4-�� �������: ������� ������� (������ ���������)
	this->position = vec3(0, 0, 0);
	this->angle = 0;
	this->color = vec4(1, 0, 0, 1);
	recalculateModelMatrix();
}

void GraphicObject::setColor(glm::vec4 color)
{
	this->color = color;
}

void GraphicObject::setPosition(glm::vec3 position)
{
	this->position = position;
	recalculateModelMatrix();
}

void GraphicObject::setAngle(float degree)
{
	this->angle = degree;
	recalculateModelMatrix();
}

void GraphicObject::setMeshId(int id)
{
	meshId = id;
}

glm::vec4& GraphicObject::getColor()
{
	return color;
}

glm::mat4& GraphicObject::getModelMatrix()
{
	return modelMatrix;
}

int GraphicObject::getMeshId()
{
	return meshId;
}

void GraphicObject::recalculateModelMatrix()
{
	modelMatrix[0][0] = cos(radians(angle));
	modelMatrix[0][1] = 0;
	modelMatrix[0][2] = sin(radians(angle));
	modelMatrix[0][3] = 0;
	modelMatrix[1][0] = 0;
	modelMatrix[1][1] = 1;
	modelMatrix[1][2] = 0;
	modelMatrix[1][3] = 0;
	modelMatrix[2][0] = -sin(radians(angle));
	modelMatrix[2][1] = 0;
	modelMatrix[2][2] = cos(angle * M_PI / 180);
	modelMatrix[2][3] = 0;
	modelMatrix[3][0] = position.x;
	modelMatrix[3][1] = position.y;
	modelMatrix[3][2] = position.z;
	modelMatrix[3][3] = 1;
}
