#pragma once

#include "GL/glew.h"
#include <windows.h>

#include <vector>
#include <iostream>
#include <math.h>


#include <GL/gl.h>
#include <GL/glu.h>
#include "GL/freeglut.h"
#include "../glm/glm/glm.hpp"
#include "../glm/glm/gtc/matrix_transform.hpp"
#include "../glm/glm/gtc/type_ptr.hpp"
// ����� ��� ������ � ����������� ��������
class GraphicObject
{
public:
	// ����������� �� ���������
	GraphicObject();
	// ���������� ���� �������
	void setColor(glm::vec4 color);
	// ���������� ������� �������
	void setPosition(glm::vec3 position);
	// ���������� ���� �������� � �������� ������������ ��� OY �� ������� �������
	void setAngle(float degree);
	// �������� ��������� ���������
	// ���������� ������������� ������������� ����
	void setMeshId(int id);

	glm::vec4& getColor();
	glm::mat4& getModelMatrix();
	int getMeshId();
private:
	// ������ ������� modelMatrix �� ������ position � angle
	void recalculateModelMatrix();
private:
	// ���� �������
	glm::vec4 color;
	// ����
	float angle;
	// ������� ������ (������ ������� � ����������)
	glm::mat4 modelMatrix;
	// �������
	glm::vec3 position;

	// ������������� ������������� ����
	int meshId;
};