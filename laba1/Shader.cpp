#include "Shader.h"
#include <fstream>
#include <string>

using namespace std;
using namespace glm;
GLuint Shader::currentProgram = 0;
bool Shader::load(string veftexShaderFilename, string fragmentShaderFilename) {
	// �������� ���������� � ������������ ��������� ��������
	GLuint vertexShader = createShaderObject(GL_VERTEX_SHADER, veftexShaderFilename);
	GLuint fragmentShader = createShaderObject(GL_FRAGMENT_SHADER, fragmentShaderFilename);

	// �������� ��������� ���������
	program = glCreateProgram();

	// ������������� � ��������� ��������� ��������� ��������
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);

	// �������� ��������� ���������
	glLinkProgram(program);

	// ��������� ������� ��������
	GLint linkStatus = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
	if (linkStatus == GL_FALSE) {
		cout << "---Error---" << endl;
		GLchar buffer[255];
		GLint sizeBuffer;
		glGetProgramInfoLog(program, 255, &sizeBuffer, &buffer[0]);
		cout << buffer << endl;
		return 0;
	}

	return 1;
}

void Shader::activate()
{
	// ����� ������� � �������� ��������
	if (program != Shader::currentProgram) {
		glUseProgram(program);
		Shader::currentProgram = program;
	}
}

void Shader::deactivate()
{
	// ����� 0 � �������� �������� �������
	glUseProgram(0);
}

void Shader::setUniform(std::string name, int value)
{
	glUniform1i(getUniformLocation(name), value);
}

void Shader::setUniform(std::string name, float value)
{
	glUniform1f(getUniformLocation(name), value);
}

void Shader::setUniform(std::string name, glm::vec2& value)
{
	glUniform2f(getUniformLocation(name), value[0], value[1]);
}

void Shader::setUniform(std::string name, glm::vec4& value)
{
	glUniform4f(getUniformLocation(name), value[0], value[1], value[2], value[3]);
}

void Shader::setUniform(std::string name, glm::mat4& value)
{
	glUniformMatrix4fv(getUniformLocation(name), 1, false, &value[0][0]);
}

GLuint Shader::createShaderObject(GLenum shaderType, std::string filename)
{
	// �������� ���������� �������
	GLuint shader = glCreateShader(shaderType);

	// ������ �� �����
	ifstream vertexFile;
	vertexFile.open(filename);
	if (!vertexFile.is_open()) {
		cout << "���� " << filename << " �� ������� �������" << endl;
		return 0;
	}
	string buffer;
	getline(vertexFile, buffer, '\0');
	const char* temp[] = { buffer.c_str() };

	// �������� ������ ������� � ��������� ��������� ������
	glShaderSource(shader, 1, temp, NULL);

	// ���������� ���������� �������
	glCompileShader(shader);

	// ��������� ������� ����������
	GLint compileStatus = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
	if (compileStatus == GL_FALSE) {
		cout << "---Error---" << endl;
		GLchar buffer[255];
		GLint sizeBuffer;
		glGetShaderInfoLog(shader, 255, &sizeBuffer, &buffer[0]);
		cout << buffer << endl;
		return 0;
	}

	return shader;
}

GLuint Shader::getUniformLocation(std::string name)
{
	activate();
	const GLchar* name2 = name.c_str();
	auto uniformPointer = uniforms.find(name);
	GLuint index = 0;
	if (uniformPointer == uniforms.end()) {
		index = uniforms[name] = glGetUniformLocation(program, name2);
	}
	else {
		index = uniformPointer->second;
	}
	return index;
}
